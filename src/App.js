import { Box, Button } from "@mui/material";
import { React, useState } from "react";
import Routers from "./Router";
import "./App.css";

function App() {
  const [trigger, setTrigger] = useState(false);

  return (
    <div className="App">
          {/* <span>My App</span> */}
          <Routers />
    </div>
  );
}

export default App;
