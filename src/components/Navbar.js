import React, { useState } from "react";
import { useHistory, NavLink } from "react-router-dom";
import { Button, Modal, TextField, Box, Alert } from "@mui/material";

const Navbar = () => {
  let history = useHistory();
  const [trigger, setTrigger] = useState(false);
  const [info, setInfo] = useState({
    name: "",
    address: "",
    hobby: "",
  });
  const [search, setSearch] = useState("");
  const [data, setData] = useState([]);
  const [error, setError] = useState("");

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <>
      <div>
        <div className="navbar-navbar">
          <p
            onClick={() => {
              history.push("/");
            }}
          >
            My App
          </p>
          <p
            onClick={() => {
              history.push("/about");
            }}
          >
            About
          </p>
          <p
            onClick={() => {
              history.push("/hobby");
            }}
          >
            Hobby
          </p>

          <div>
            <Button
              onClick={() => {
                setTrigger(true);
              }}
              sx={{ borderRadius: 28 }}
              variant="contained"
            >
              Add User
            </Button>

            <Modal
              open={trigger}
              onClose={() => setTrigger(false)}
              aria-labelledby="parent-modal-title"
              aria-describedby="parent-modal-description"
            >
              <Box sx={{ ...style, width: 400 }}>
                {error ? (
                  <Alert severity="error">{error} field cannot be empty</Alert>
                ) : (
                  <></>
                )}
                <label>Name</label>
                <TextField
                  required
                  sx={{
                    borderRadius: 28,
                    paddingBottom: "10px",
                    paddingTop: "10px",
                  }}
                  fullWidth
                  variant="outlined"
                  onChange={(e) => setInfo({ ...info, name: e.target.value })}
                />
                <label>Address</label>
                <TextField
                  required
                  sx={{
                    borderRadius: 28,
                    paddingBottom: "10px",
                    paddingTop: "10px",
                  }}
                  fullWidth
                  variant="outlined"
                  onChange={(e) =>
                    setInfo({ ...info, address: e.target.value })
                  }
                />
                <label>Hobby</label>
                <TextField
                  required
                  sx={{
                    borderRadius: 28,
                    paddingBottom: "10px",
                    paddingTop: "10px",
                  }}
                  fullWidth
                  variant="outlined"
                  onChange={(e) => setInfo({ ...info, hobby: e.target.value })}
                />
                <div className="btn-save">
                  <Button
                    variant="contained"
                    onClick={() => {
                      if (info.name == "") {
                        setError("Name");
                      } else if (info.address == "") {
                        setError("Adress");
                      } else if (info.hobby == "") {
                        setError("Hobby");
                      } else {
                        setError("");
                        setData((prev) => [...prev, info]);
                        setTrigger(false);
                        setInfo({
                          name: "",
                          address: "",
                          hobby: "",
                        });
                      }
                    }}
                  >
                    <NavLink
                      to={{
                        pathname: "/",
                        state: {
                          name: info.name,
                          address: info.address,
                          hobby: info.hobby,
                        },
                        flag: "Add"
                      }}
                      activeStyle={{ color: "white" }}
                    >
                      Save
                    </NavLink>
                  </Button>
                </div>
              </Box>
            </Modal>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
