import React from "react";
import Navbar from "../components/Navbar";

const About = () => {
  return (
    <>
      <div className="navbar">
        <Navbar />
      </div>
      <p>
        Aplikasi yang dibuat merupakan aplikasi untuk menambahkan user yang
        berisi tentang nama, alamat, dan hobby. Ketika user sudah ditambahkan,
        maka akan terlihat list pada tabel tersebut. Kemudian terdapat fitur
        edit jika ingin mengubah nama, alamat, atau hobby. Kemudian terdapat
        fitur search untuk mencari user berdasarkan nama yang ingin dicari.
        Kemudian fitur view untuk melihat hobby.
      </p>
    </>
  );
};

export default About;
