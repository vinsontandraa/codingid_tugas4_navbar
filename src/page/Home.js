import React from "react";
import Navbar from "../components/Navbar";
import Button from "../components/Button";
import { useLocation } from "react-router-dom";
const Home = () => {
    const list = useLocation();
  return (
    <>
      <div className="navbar">
        <Navbar />
      </div>
      <div>
        <Button />
      </div>
      {/* <div>
      <h1>nama saya : {list.state.namaSaya}</h1>
      <h1>umur saya : {list.state.umurSaya}</h1>
      </div> */}
    </>
  );
};

export default Home;
