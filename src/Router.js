import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import About from './page/About'
import Hobby from './page/Hobby'
import Home from './page/Home'

const Routers = () => {
  return (
    <div>
      <Router>
        <Switch>
            <Route exact path={'/'}>
                <Home/>
            </Route>

            <Route exact path={"/about"}>
                <About/>
            </Route>

            <Route exact path={"/hobby"}>
                <Hobby/>
            </Route>
        </Switch>
      </Router>
    </div>
  )
}

export default Routers
